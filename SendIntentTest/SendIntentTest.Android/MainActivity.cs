﻿using Android.App;
using Android.Content.PM;
using Android.OS;
using Prism;
using Prism.Ioc;
using static Android.Content.ClipData;

namespace SendIntentTest.Droid
{
    [Activity(Label = "SendIntentTest", Icon = "@mipmap/ic_launcher", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    [IntentFilter(new[] { Android.Content.Intent.ActionSend },
                  Categories = new [] { Android.Content.Intent.CategoryDefault },
                  DataMimeType = @"image/*")]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);
            var mainForms = new App(new AndroidInitializer());
            LoadApplication(mainForms);

            if (Intent.Action == Android.Content.Intent.ActionSend)
            {
                Item imageClipData = Intent.ClipData.GetItemAt(0);

                System.IO.Stream imgStream = ContentResolver.OpenInputStream(imageClipData.Uri);
                var imgMemStream = new System.IO.MemoryStream();
                imgStream.CopyTo(imgMemStream);
                string documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                string imgSavePath = System.IO.Path.Combine(documentsPath, "img1.png");
                System.IO.File.WriteAllBytes(imgSavePath, imgMemStream.ToArray());

                //mainForms.HandleImage(imgSavePath);

                mainForms.HandleImage(new System.IO.FileStream(imgSavePath, System.IO.FileMode.Open, System.IO.FileAccess.Read));
            }
        }
    }

    public class AndroidInitializer : IPlatformInitializer
    {
        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            // Register any platform specific implementations
        }
    }
}

