﻿using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace SendIntentTest.ViewModels
{
    public class ImageDisplayPageViewModel : ViewModelBase
    {
        private ImageSource _imageToDisplay;
        public ImageSource ImageToDisplay
        {
            get { return this._imageToDisplay; }
            set { this.SetProperty(ref this._imageToDisplay, value, "ImageToDisplay"); }
        }

        public ImageDisplayPageViewModel(INavigationService navService)
            : base(navService)
        {
        }

        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            //string imageUrl = parameters.GetValue<string>("ImageUrl");
            //ImageSource img = ImageSource.FromUri(new Uri(imageUrl));
            //this.ImageToDisplay = img;

            System.IO.Stream imgStream = parameters.GetValue<System.IO.Stream>("ImageStream");
            ImageSource img = ImageSource.FromStream(() => imgStream);
            this.ImageToDisplay = img;
        }
    }
}
