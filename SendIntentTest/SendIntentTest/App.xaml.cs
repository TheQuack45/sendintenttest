﻿using Prism;
using Prism.Ioc;
using Prism.Navigation;
using SendIntentTest.ViewModels;
using SendIntentTest.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace SendIntentTest
{
    public partial class App
    {
        /* 
         * The Xamarin Forms XAML Previewer in Visual Studio uses System.Activator.CreateInstance.
         * This imposes a limitation in which the App class must have a default constructor. 
         * App(IPlatformInitializer initializer = null) cannot be handled by the Activator.
         */
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }

        protected override async void OnInitialized()
        {
            InitializeComponent();

            await NavigationService.NavigateAsync("NavigationPage/MainPage");
            //await NavigationService.NavigateAsync("NavigationPage/ImageDisplayPage");
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<MainPage, MainPageViewModel>();
            containerRegistry.RegisterForNavigation<ImageDisplayPage, ImageDisplayPageViewModel>();
        }

        public void HandleImage(string url)
        {
            var parameters = new NavigationParameters();
            parameters.Add("ImageUrl", url);

            this.NavigationService.NavigateAsync("NavigationPage/ImageDisplayPage", parameters);
        }

        public void HandleImage(System.IO.Stream imgStream)
        {
            var parameters = new NavigationParameters();
            parameters.Add("ImageStream", imgStream);

            this.NavigationService.NavigateAsync("NavigationPage/ImageDisplayPage", parameters);
        }
    }
}
